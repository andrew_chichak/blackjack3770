#ifndef DECK_H
#define DECK_H
#include "Hand.h"
#include "Card.h"
#include <vector>
class Deck : public Hand{

public:
	Deck();

	virtual ~Deck();

	void createDeck();
	void shuffleDeck();

	Card* dealCard();

	//QString additionalCards(AbstractPlayer& aAbstractPlayer);
  protected:
		std::vector<Card*> cards;



};





#endif
