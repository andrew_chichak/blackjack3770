#include "Hand.h"

Hand::Hand(){
	cards.reserve(5);
}

Hand::~Hand(){
	newHand();
}

void Hand::hitMe(Card *newCard){

	cards.push_back(newCard);

}
int Hand::handValue(){

   int y=0;
    int numAces=0;
    for (int i=0;i<cards.size();i++){
       if(cards[i]->getCardVal()==1){
            numAces++;
            y+=10;
        }
        y+=cards[i]->getCardVal();
    }
    for (int i=0; i<numAces; i++) {
        if (y>21) {
            y-=10;
        }
    }
    return y;
}

void Hand::newHand(){
    for (int i = 0; i < cards.size(); ++i)
        {
            delete cards.back();
            cards.pop_back();
        }
}
