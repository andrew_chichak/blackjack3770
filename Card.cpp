#include "Card.h"

Card::Card(Rank r, Suit s, bool isShown) : rank(r), suit(s), isCardShown(isShown){}

int Card::getCardVal() const{
	//:: Default Value of zero when face down.
	int cardValue = 0;

switch (rank){
		case ACE:
		cardValue = 1;
		break;
		case TWO:
		cardValue = 2;
		break;
		case THREE:
		cardValue = 3;
		break;
		case FOUR:
		cardValue = 4;
		break;
		case FIVE:
		cardValue = 5;
		break;
		case SIX:
		cardValue = 6;
		break;
		case SEVEN:
		cardValue = 7;
		break;
		case EIGHT:
		cardValue = 8;
		break;
		case NINE:
		cardValue = 9;
		break;
		case TEN:
		cardValue = 10;
		break;
		case JACK:
		cardValue = 10;
		break;
		case QUEEN:
		cardValue = 10;
		break;
		case KING:
		cardValue = 10;
		break;
	}

	return cardValue;

}

QString Card::getImage(){
	if (!isCardShown)
	{
		return("./images/deck/back.png");
	}
	QString x="./images/deck/";
	QString s="";
	QString s2="";
	QString r="";

	switch (suit){
	case SPADE:
	s="Spades";
	s2="spade";
	break;
	case CLUB:
	s="Clubs";
	s2="club";
	break;
	case HEART:
	s="Hearts";
	s2="heart";
	break;
	case DIAMOND:
	s="Diamonds";
	s2="diamond";
	break;
	}

	switch (rank){
		case ACE:
		r="1";
		break;
		case TWO:
		r="2";
		break;
		case THREE:
		r="3";
		break;
		case FOUR:
		r="4";
		break;
		case FIVE:
		r="5";
		break;
		case SIX:
		r="6";
		break;
		case SEVEN:
		r="7";
		break;
		case EIGHT:
		r="8";
		break;
		case NINE:
		r="8";
		break;
		case TEN:
		r="10";
		break;
		case JACK:
		r="jack";
		break;
		case QUEEN:
		r="queen";
		break;
		case KING:
		r="king";
		break;
	}

	x.append(s);
	x.append("/");
	x.append(r);
	x.append("_");
	x.append(s2);
	x.append(".png");
	return x;
}


void Card::flipCardOver(){

	isCardShown = !isCardShown;

}
