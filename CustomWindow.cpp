/**
*	Purpose: To provide implementation details for the CustomWindow Object
*	Filename: CustomWindow.cpp
*
*	Author: Tylor Faoro - tylor.faoro@uleth.ca
*/

#include <QtGui>
#include "CustomWindow.h"
#include "Hand.h"
#include <QApplication>

CustomWindow::CustomWindow(){
	dealer = new Player();
	cards = new Deck();
	cards->shuffleDeck();
	user = new Hand();

	QFont titleFont("Brioso", 28);
	QColor whiteText("#FFFFFF");
	QWidget *centralWidget = new QWidget;
	setCentralWidget( centralWidget );

	titleBanner = new QLabel("Blackjack");
	titleBanner->setFixedHeight(50);
	titleBanner->setAlignment(Qt::AlignCenter);
	titleBanner->setFont( titleFont );
	titleBanner->setStyleSheet("QLabel{color: white; font-weight: bold; text-align: left }");

	banner = new QLabel("");
	banner ->setFixedHeight(70);
	banner->setFont( titleFont );
	banner->setStyleSheet("QLabel{color: white; font-weight: bold; text-align: left }");


	QVBoxLayout *container 		= new QVBoxLayout;
	QHBoxLayout *top		= new QHBoxLayout;
	
	QHBoxLayout	*middle		= new QHBoxLayout;
	QVBoxLayout *middleLeft		= new QVBoxLayout;
	QVBoxLayout *middleMiddle	= new QVBoxLayout;
	QVBoxLayout *middleRight	= new QVBoxLayout;
	
	QHBoxLayout	*bottom		= new QHBoxLayout;
	QVBoxLayout *bottomLeft		= new QVBoxLayout;
	QVBoxLayout *bottomMiddle	= new QVBoxLayout;
	QVBoxLayout *bottomRight	= new QVBoxLayout;

	quitButton = new QPushButton("Quit");
	quitButton->setFixedWidth(150);
	connect(quitButton, SIGNAL(clicked()), this, SLOT( close()));
	
	hitButton = new QPushButton("Hit",this);
	hitButton->setGeometry(QRect(QPoint(100,293),QSize(50, 50)));
	hitButton->setVisible(false);
	connect(hitButton,SIGNAL(clicked()),this,SLOT(hitButton_clicked()));

	stayButton = new QPushButton("Stay",this);
	stayButton->setGeometry(QRect(QPoint(100,353),QSize(50, 50)));
	stayButton->setVisible(false);
	connect(stayButton,SIGNAL(clicked()),this,SLOT(stayButton_clicked()));


	newGameButton = new QPushButton("New Game");
	newGameButton->setFixedWidth(150);
	connect(newGameButton,SIGNAL(clicked()),this,SLOT(newGameButton_clicked()));



	spacerWidget = new QLabel("");
	spacerWidget->setFixedWidth(150);

		
	middleLeft->addWidget(spacerWidget);
	middleMiddle->addWidget(spacerWidget);
	middleRight->addWidget(spacerWidget);

	top->addWidget( titleBanner );
	middleRight->addWidget( banner );
	middle->addLayout(middleLeft);
	middle->addLayout(middleMiddle);

	middle->addLayout(middleRight);
	
	bottomMiddle->addWidget(newGameButton);	
	bottomMiddle->addWidget(quitButton);
	

	bottom->addLayout(bottomLeft);
	bottom->addLayout(bottomMiddle);
	bottom->addLayout(bottomRight);

	container->addLayout(top);
	container->addLayout(middle);
	container->addLayout(bottom);

	centralWidget->setLayout(container);



}
void CustomWindow::hitButton_clicked(){
	Card *x = cards->dealCard();
	x->flipCardOver();
	user->cards.push_back(x);

	int userHand = user->handValue();
	int dealerHand = dealer->hand.handValue();
	if(userHand>21){
		banner->setText("BUST");
		hitButton->setVisible(false);
		stayButton->setVisible(false);
	}

	else if(dealerHand>21){
		banner->setText("WIN");
		hitButton->setVisible(false);
		stayButton->setVisible(false);
	}

	if (dealer->musthit()){
		if (dealer->hand.cards[1]->getImage()=="./images/deck/back.png")
			dealer->hand.cards[1]->flipCardOver();
		x = cards->dealCard();
		x->flipCardOver();
		dealer->hand.cards.push_back(x);
	}
	this->repaint();



}
void CustomWindow::stayButton_clicked(){
	Card *x;
	while(dealer->musthit()){
		
			if (dealer->hand.cards[1]->getImage()=="./images/deck/back.png")
			dealer->hand.cards[1]->flipCardOver();
		
			x = cards->dealCard();
			x->flipCardOver();
			dealer->hand.cards.push_back(x);
			this->repaint();

	}
	if (dealer->hand.cards[1]->getImage()=="./images/deck/back.png")
		dealer->hand.cards[1]->flipCardOver();
	this->repaint();

	int userHand = user->handValue();
	int dealerHand = dealer->hand.handValue();

	if(dealerHand>21){
		banner->setText("WIN");
		hitButton->setVisible(false);
		stayButton->setVisible(false);
		return;
	}

	else if(dealerHand>userHand){
		QString q = QString::number(dealerHand);
		q.append(" ");
		q.append(QString::number(userHand));
		banner->setText("LOSE");
		hitButton->setVisible(false);
		stayButton->setVisible(false);
	}
	else if(dealerHand<userHand){
		banner->setText("WIN");
		hitButton->setVisible(false);
		stayButton->setVisible(false);
	}
	else if(dealerHand==userHand){
		banner->setText("TIE");
		hitButton->setVisible(false);
		stayButton->setVisible(false);
	}

}

void CustomWindow::newGameButton_clicked(){
		hitButton->setVisible(true);
		stayButton->setVisible(true);
		delete dealer;
		delete cards;
		delete user;
		banner->setText("");
		dealer = new Player();
		cards = new Deck();
		cards->shuffleDeck();
		user = new Hand();

		Card *x = cards->dealCard();
		x->flipCardOver();
		user->cards.push_back(x);

		x = cards->dealCard();
		x->flipCardOver();
		user->cards.push_back(x);

		x = cards->dealCard();
		x->flipCardOver();
		dealer->hand.cards.push_back(x);

		x = cards->dealCard();
		dealer->hand.cards.push_back(x);

		this->repaint();
}


void CustomWindow::paintEvent(QPaintEvent *){
    QPixmap bkgnd("./images/tableBackground.jpeg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
   
	QPainter painter(this);
	QPixmap cards("./images/Ace_Jack.png");
	painter.drawPixmap(705, 5, 250, 100, cards );
	
	for (int i = 0; i < user->cards.size(); ++i)
	{
		QPainter painter(this);
		QPixmap cards(user->cards[i]->getImage());
		painter.drawPixmap((160+i*28), 293, 76, 124, cards );
	}
	for (int i = 0; i < dealer->hand.cards.size(); ++i)
	{
		QPainter painter(this);
		QPixmap cards(dealer->hand.cards[i]->getImage());
		painter.drawPixmap((160+i*28), 149, 76, 124, cards );
	}

}

