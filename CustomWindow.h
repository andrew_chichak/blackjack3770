/**
*	Purpose: Declaration of the CustomWindow Object. This Object is an extension 
*			 of the QMainWindow Library.
*	Filename: CustomWindow.h
*
*	Author: Tylor Faoro - tylor.faoro@uleth.ca
*/

#ifndef CUSTOMWINDOW_H
#define CUSTOMWINDOW_H

#include <QMainWindow>
#include "Hand.h"
#include "Player.h"
#include "Deck.h"


class QVBoxLayout;
class QHBoxLayout;
class QLabel;
class QPushButton;
class CustomWindow : public QMainWindow{
Q_OBJECT

public:
	CustomWindow();

protected:
	void paintEvent(QPaintEvent *event);

private:
	QLabel *titleBanner;
	QLabel *banner;
	QLabel *spacerWidget;
	QPushButton *newGameButton;
	QPushButton *quitButton;
	QPushButton *hitButton;
	QPushButton *stayButton;
	Deck* cards;
	Hand* user;
	Player* dealer;

private slots:
	void hitButton_clicked();
	void newGameButton_clicked();
	void stayButton_clicked();
};

#endif
