#ifndef CARD_H
#define CARD_H

#include <QString>
#include <QWidget>
#include <iostream>

class Card{

public:
	enum Rank{ACE = 1, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};
	enum Suit{SPADE, CLUB, HEART, DIAMOND};

	Card(Rank r = ACE, Suit s = SPADE, bool cardShown = true);
	int getCardVal() const;
	QString getImage();
	void flipCardOver();

	//string convert();

private:
	Rank rank;
	Suit suit;
	bool isCardShown;

};

#endif

