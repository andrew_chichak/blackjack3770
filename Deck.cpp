#include "Deck.h"
#include <algorithm>
#include <ctime>
Deck::Deck(){
	cards.reserve(52);
	createDeck();

}

void Deck::createDeck(){
	cards.push_back(new Card(Card::ACE,Card::SPADE,false));
	cards.push_back(new Card(Card::ACE,Card::CLUB,false));
	cards.push_back(new Card(Card::ACE,Card::HEART,false));
	cards.push_back(new Card(Card::ACE,Card::DIAMOND,false));

	cards.push_back(new Card(Card::TWO,Card::SPADE,false));
	cards.push_back(new Card(Card::TWO,Card::CLUB,false));
	cards.push_back(new Card(Card::TWO,Card::HEART,false));
	cards.push_back(new Card(Card::TWO,Card::DIAMOND,false));

	cards.push_back(new Card(Card::THREE,Card::SPADE,false));
	cards.push_back(new Card(Card::THREE,Card::CLUB,false));
	cards.push_back(new Card(Card::THREE,Card::HEART,false));
	cards.push_back(new Card(Card::THREE,Card::DIAMOND,false));

	cards.push_back(new Card(Card::FOUR,Card::SPADE,false));
	cards.push_back(new Card(Card::FOUR,Card::CLUB,false));
	cards.push_back(new Card(Card::FOUR,Card::HEART,false));
	cards.push_back(new Card(Card::FOUR,Card::DIAMOND,false));

	cards.push_back(new Card(Card::FIVE,Card::SPADE,false));
	cards.push_back(new Card(Card::FIVE,Card::CLUB,false));
	cards.push_back(new Card(Card::FIVE,Card::HEART,false));
	cards.push_back(new Card(Card::FIVE,Card::DIAMOND,false));

	cards.push_back(new Card(Card::SIX,Card::SPADE,false));
	cards.push_back(new Card(Card::SIX,Card::CLUB,false));
	cards.push_back(new Card(Card::SIX,Card::HEART,false));
	cards.push_back(new Card(Card::SIX,Card::DIAMOND,false));

	cards.push_back(new Card(Card::SEVEN,Card::SPADE,false));
	cards.push_back(new Card(Card::SEVEN,Card::CLUB,false));
	cards.push_back(new Card(Card::SEVEN,Card::HEART,false));
	cards.push_back(new Card(Card::SEVEN,Card::DIAMOND,false));

	cards.push_back(new Card(Card::EIGHT,Card::SPADE,false));
	cards.push_back(new Card(Card::EIGHT,Card::CLUB,false));
	cards.push_back(new Card(Card::EIGHT,Card::HEART,false));
	cards.push_back(new Card(Card::EIGHT,Card::DIAMOND,false));

	cards.push_back(new Card(Card::NINE,Card::SPADE,false));
	cards.push_back(new Card(Card::NINE,Card::CLUB,false));
	cards.push_back(new Card(Card::NINE,Card::HEART,false));
	cards.push_back(new Card(Card::NINE,Card::DIAMOND,false));

	cards.push_back(new Card(Card::TEN,Card::SPADE,false));
	cards.push_back(new Card(Card::TEN,Card::CLUB,false));
	cards.push_back(new Card(Card::TEN,Card::HEART,false));
	cards.push_back(new Card(Card::TEN,Card::DIAMOND,false));

	cards.push_back(new Card(Card::JACK,Card::SPADE,false));
	cards.push_back(new Card(Card::JACK,Card::CLUB,false));
	cards.push_back(new Card(Card::JACK,Card::HEART,false));
	cards.push_back(new Card(Card::JACK,Card::DIAMOND,false));

	cards.push_back(new Card(Card::QUEEN,Card::SPADE,false));
	cards.push_back(new Card(Card::QUEEN,Card::CLUB,false));
	cards.push_back(new Card(Card::QUEEN,Card::HEART,false));
	cards.push_back(new Card(Card::QUEEN,Card::DIAMOND,false));

	cards.push_back(new Card(Card::KING,Card::SPADE,false));
	cards.push_back(new Card(Card::KING,Card::CLUB,false));
	cards.push_back(new Card(Card::KING,Card::HEART,false));
	cards.push_back(new Card(Card::KING,Card::DIAMOND,false));


}

void Deck::shuffleDeck(){
	std::srand(std::time(0));
	std::random_shuffle( cards.begin(), cards.end() );

}

Card*  Deck::dealCard(){
	if( !cards.empty() ){
		Card* x = cards.back();
		cards.pop_back();
		return x;
	}
	else{
			//:: Output Out of Cards

	}

}

Deck::~Deck(){}

