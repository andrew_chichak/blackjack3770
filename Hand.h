#ifndef HAND_H
#define HAND_H

#include "Card.h"
#include <vector>

class Hand{
  public:
	Hand();

	~Hand();

	void hitMe(Card *newCard);
	int handValue();
	void newHand();
	std::vector<Card*> cards;


};


#endif
